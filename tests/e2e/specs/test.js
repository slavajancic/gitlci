// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('https://gitlab.com/slavajancic/gitlci.git')
    cy.get('#email').invoke('attr', 'value', 'test1@gmail.com').should('have.value', 'test@gmail.com')
    cy.get('#password').invoke('attr', 'value', '12345').should('have.value', '12345')
    cy.get("#submit").click()
  })
})